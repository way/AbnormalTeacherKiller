package hangxingliu.java.atk;

/**
 * 存放程序所需常量
 * 
 * @author VoyageLiu
 * @version 1.0
 */
public final class Const {
	
	public static final String PWD = "ywlsmmd";//语文老师萌萌的...
	
	public static final String VERSION = " 1.0.0-test";
	
	public static final int PORT_UDP_RECEIVE = 9711;
	
	public static final int PORT_UDP_CLIENT = 9701;
	
	public static final int PORT_TCP_CLIENT_FILE_LIST = 9702;
	
	public static final int PORT_TCP_CLIENT_FILE_DOWNLOAD = 9703;
	
	public static final String CMD_KILL_ATK = "atk kill";
	
	public static final String CMD_SHOW_ATK = "atk show";
	
	public static final long TIME_INTERVAL_CLIPBOARD = 500;
	
	public static final int SIZE_UDP_DATA = 1024;
}
