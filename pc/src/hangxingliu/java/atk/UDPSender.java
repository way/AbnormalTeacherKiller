package hangxingliu.java.atk;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public final class UDPSender {
	private final static void _Send(byte[] data,int length,InetAddress address,int port) throws IOException{
		DatagramPacket dp = new DatagramPacket(data,0,length,address,port);
		DatagramSocket ds = new DatagramSocket();
		ds.send(dp);
		ds.close();
	}
	
	public final static boolean Send(byte[] data,int length,InetAddress address,int port){
		try {
			_Send(data, length, address, port);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
