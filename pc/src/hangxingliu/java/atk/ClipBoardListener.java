package hangxingliu.java.atk;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;

/**
 * �������а�����ȡATK��������
 * 
 * @author VoyageLiu
 * @version
 */
public final class ClipBoardListener implements Runnable{
	
	private Clipboard cb =null;
	
	private Transferable tf;
	
	private Thread thread;
	
	private boolean continueListen;
	
	private String tmpStr;
	
	public ClipBoardListener() {
		
		cb = Toolkit.getDefaultToolkit().getSystemClipboard();
		
		continueListen = true;
		
		thread = new Thread(this);
		thread.start();
	}
	
	private String getSystemClipboard(){
	    try {   
		    tf = cb.getContents(null);  
	        if (tf != null && tf.isDataFlavorSupported(DataFlavor.stringFlavor)) {   
	        	return (String)tf.getTransferData(DataFlavor.stringFlavor);   
	        }   
	    } catch (Exception e) {  
	    	return "";
	    }
	    return "";
	} 
	
	private void setSystemClipboard(String content){
		try{
			StringSelection ss = new StringSelection(content);  
			cb.setContents(ss,null);  
		}catch(Exception e){
			return ;
		}
	}

	@Override
	public void run() {
		while(continueListen){
			threadSleep();
			if(!continueListen)
				break;
			tmpStr = getSystemClipboard();
			tmpStr = tmpStr == null?"":tmpStr.trim().toLowerCase();
			if(tmpStr.length() > 0){
				if(tmpStr.equals("atk kill")){
					setSystemClipboard("");
					Launcher.killATKService();
					
					return ;
				}
				if(tmpStr.equals("atk show")){
					setSystemClipboard("");
					Launcher.showATKWin();
					
					continue;
				}
			}
		}
	} 
	
	private void threadSleep(){
		try {
			Thread.sleep(Const.TIME_INTERVAL_CLIPBOARD);
		} catch (InterruptedException e) {
			
		}
	}
	
//	@SuppressWarnings("deprecation")
	public void stopListener(){
		continueListen = false;
		//thread.stop();
		cb = null;
		tf = null;
		thread = null;
	}
}
