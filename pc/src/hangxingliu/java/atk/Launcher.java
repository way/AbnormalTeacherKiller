package hangxingliu.java.atk;

public class Launcher {

	public static ATKWin atkWin = null;
	
	public static ClipBoardListener cbListener = null;
	
	public static MainServer server;
	
	/**
	 * 启动ATK,干掉该死的语文老师
	 * @param args
	 */
	public static void main(String[] args) {
		
		Log.addLog("ATK服务正在启动...");
				
		server = new MainServer();
		
		cbListener = new ClipBoardListener();
		
		atkWin = new ATKWin();
		atkWin.setVisible(true);
		
		Log.addLog("ATK服务启动成功!");
	}

	
	/**
	 * 结束ATK服务
	 */
	public static void killATKService(){
		try{
			if(server != null){
				server.stop();
				server = null;
			}
			if(atkWin != null){
				atkWin.dispose();
				atkWin = null;
			}
			if(cbListener != null){
				cbListener.stopListener();
				cbListener = null;
			}
		}catch(Exception e){
			
		}
		System.gc();
		System.exit(0);
	}
	
	/**
	 * 显示ATK界面
	 */
	public static void showATKWin(){
		if(atkWin != null){
			atkWin.setVisible(true);
		}
	}
}
