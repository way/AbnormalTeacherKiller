package hangxingliu.java.atk.sysaction;

import hangxingliu.java.atk.Log;

import java.awt.AWTException;

public final class MouseKeyboard {
	
	private static VLRobot robot = null;
	
	public static void mouse(int action){
		if(robot == null){
			try {
				robot = new VLRobot();
			} catch (AWTException e) {
				Log.addLog("初始化Robot类出错:\n".concat(e.getMessage()));
				return ;
			};
		}
		Log.addLog("UDP请求鼠标动作:".concat(Integer.toString(action)));
		switch(action){
		case 0:
			//左键
			robot.VClickMouse('L');
			break;
		case 1:
			//右键
			robot.VClickMouse('R');
			break;
		case 2:
			//双击
			robot.VClickMouse('L');
			robot.delay(100);
			robot.VClickMouse('L');
			break;
		case 3:
			//向上滚11
			robot.mouseWheel(-11);
			break;
		case 4:
			//向上滚5
			robot.mouseWheel(-5);
			break;
		case 5:
			//向上滚2
			robot.mouseWheel(-2);
			break;
		case 6:
			//向下滚11
			robot.mouseWheel(11);
			break;
		case 7:
			//向下滚5
			robot.mouseWheel(5);
			break;
		case 8:
			//向下滚2
			robot.mouseWheel(2);
			break;
		}
	}
	
	public static void keyboard(int head,int action){
		if(robot == null){
			try {
				robot = new VLRobot();
			} catch (AWTException e) {
				Log.addLog("初始化Robot类出错:\n".concat(e.getMessage()));
				return ;
			};
		}
		Log.addLog("UDP请求键盘动作:".concat(Integer.toString(head))
				.concat("-").concat(Integer.toString(action)));
		if(head == 0){
			GroupKeys(action);
			return ;
		}
		if(head == 1){
			String c = Character.toString(((char) action)).toLowerCase();
			robot.VInputString(c);
		}
	}

	private static final int KEYCODE_WIN = 524;
	private static final int KEYCODE_CTRL = 17;
	private static final int KEYCODE_SHIFT = 16;
	private static final int KEYCODE_ALT = 18;
	private static final int KEYCODE_DELETE = 127;
	
	private static void GroupKeys(int action){
		int[] keys = null;
		switch(action){
		case 0:
			//Win+D
			keys = new int[]{KEYCODE_WIN,68};
			break;
		case 1:
			//Win+L
			keys = new int[]{KEYCODE_WIN,76};
			break;
		case 2:
			//Win+R
			keys = new int[]{KEYCODE_WIN,82};
			break;
		case 13:
			//Ctrl+C
			keys = new int[]{KEYCODE_CTRL,67};
			break;
		case 14:
			//Ctrl+X
			keys = new int[]{KEYCODE_CTRL,88};
			break;
		case 15:
			//Ctrl+V
			keys = new int[]{KEYCODE_CTRL,86};
			break;
		case 18:
			//Ctrl+Shift
			keys = new int[]{KEYCODE_CTRL,KEYCODE_SHIFT};
			break;
		case 19:
			//Ctrl+Space
			keys = new int[]{KEYCODE_CTRL,32};
			break;
		case 20:
			//Ctrl+Alt+Del
			keys = new int[]{KEYCODE_CTRL,KEYCODE_ALT,KEYCODE_DELETE};
			break;
		case 21:
			//Ctrl+P
			keys = new int[]{KEYCODE_CTRL,80};
			break;
		case 22:
			//Ctrl+A
			keys = new int[]{KEYCODE_CTRL,65};
			break;
		case 30:
			//Alt+Tab
			keys = new int[]{KEYCODE_ALT,9};
			break;
		case 40:
			//Arrow UP
			keys = new int[]{38};
			break;
		case 41:
			//Arrow DOWN
			keys = new int[]{40};
			break;
		case 42:
			//Arrow LEFT
			keys = new int[]{37};
			break;
		case 43:
			//Arrow RIGHT
			keys = new int[]{39};
			break;
		case 50:
			//Tab
			keys = new int[]{9};
			break;
		case 51:
			//Caps Lock
			keys = new int[]{20};
			break;
		case 52:
			//Shift
			keys = new int[]{KEYCODE_SHIFT};
			break;
		case 53:
			//Ctrl
			keys = new int[]{KEYCODE_CTRL};
			break;
		case 54:
			//Win
			keys = new int[]{KEYCODE_WIN};
			break;
		case 55:
			//Alt
			keys = new int[]{KEYCODE_ALT};
			break;
		case 56:
			//Enter
			keys = new int[]{13};
			break;
		case 57:
			//Space
			keys = new int[]{32};
			break;
		}
		if(keys != null){
			robot.VKnockKey(keys);
		}
	}
}
