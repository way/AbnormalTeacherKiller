package hangxingliu.java.atk;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JPasswordField;

public class ATKWin extends JDialog {

	private static final long serialVersionUID = 5109751118809595983L;
	
	private final JPanel contentPanel = new JPanel();
	private JButton btnCleanLog;
	private JScrollPane scrollLog;
	private JTextPane txtLog;
	private JButton btnKillATK;
	
	private Thread threadRefresh;
	
	private boolean continueRefresh = true;
	
	private boolean pauseRefresh = false;
	private JButton btnPause;
	private JPanel panel;
	private JPasswordField pwd;
	private JButton btnUnLock;
	
	public ATKWin() {
		setTitle("ATK".concat(Const.VERSION));
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		
		scrollLog = new JScrollPane();
		getContentPane().add(scrollLog, BorderLayout.CENTER);
		
		txtLog = new JTextPane();
		scrollLog.setViewportView(txtLog);
		
		{
			JPanel paneBottom = new JPanel();
			paneBottom.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(paneBottom, BorderLayout.SOUTH);
			
			btnCleanLog = new JButton("清空日志");
			btnCleanLog.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Log.cleanLog();
				}
			});
			
			btnPause = new JButton("暂停记录日志");
			btnPause.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(pauseRefresh){
						btnPause.setText("暂停记录日志");
					}else{
						btnPause.setText("继续记录日志");
					}
					pauseRefresh = !pauseRefresh;
				}
			});
			
			paneBottom.add(btnPause);
			paneBottom.add(btnCleanLog);
			
			btnKillATK = new JButton("关闭ATK服务");
			btnKillATK.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Launcher.killATKService();
				}
			});
			paneBottom.add(btnKillATK);
			
		}
		
		panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		pwd = new JPasswordField();
		pwd.setPreferredSize(new Dimension(200, 30));
		pwd.setEchoChar('♀');
		panel.add(pwd);
		
		btnUnLock = new JButton("显示日志");
		btnUnLock.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String key = new String(pwd.getPassword());
				pwd.setText("");
				if(key.equals(Const.PWD)){
					panel.setVisible(false);
					txtLog.setVisible(true);
				}else{
					JOptionPane.showMessageDialog(ATKWin.this,
							"口令错误!","天王盖地虎",JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnUnLock.setPreferredSize(new Dimension(90, 30));
		panel.add(btnUnLock);
		
		threadRefresh = new Thread(new RefreshLog());
		continueRefresh = true;
		threadRefresh.start();
	}
	
	//@SuppressWarnings("deprecation")
	@Override
	public void dispose() {
		continueRefresh = false;
		//threadRefresh.stop();
		threadRefresh = null;
		super.dispose();
	}
	
	@Override
	public void setVisible(boolean b) {
		if(b){
			panel.setVisible(true);
			txtLog.setVisible(false);
		}
		super.setVisible(b);
	}
	
	
	private final class RefreshLog implements Runnable{

		@Override
		public void run() {
			String log = null;
			while(continueRefresh){
				if(!pauseRefresh){
					log = Log.getLog();
					txtLog.setText(log);
					txtLog.setSelectionStart(log.length());
					txtLog.setSelectionEnd(log.length());
				}
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
	}

}
