package hangxingliu.java.atk;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 程序日志存放类
 * 
 * @author VoyageLiu
 * @version 1.0
 */
public class Log {

	private static String logData = null;
	
	private static final SimpleDateFormat DATE_FORMAT =
			new SimpleDateFormat("yy-MM-dd HH:mm:ss");
	
	public static void addLog(String log){
		if(logData == null)
			logData = "";
		log = "\ntime:"
				.concat(DATE_FORMAT.format(new Date()))
				.concat("\n\t")
				.concat(log);
		logData = logData.concat(log);
	}
	
	public static void cleanLog(){
		logData = "";
	}
	
	public static String getLog(){
		if(logData == null)
			logData = "";
		return logData;
	}
}
