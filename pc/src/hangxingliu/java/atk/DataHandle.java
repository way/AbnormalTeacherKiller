package hangxingliu.java.atk;

import hangxingliu.java.atk.sysaction.FileManager;
import hangxingliu.java.atk.sysaction.MouseKeyboard;

import java.net.DatagramPacket;

public class DataHandle {

	private static int MARK_IAMONLINE = 88;
	
	public static void handle(DatagramPacket dp){
		Log.addLog("正在处理UDP请求数据...");
		byte[] dat = dp.getData();
		int datLength = dp.getLength();
		if(datLength < 7){
			Log.addLog("UDP请求数据非法!");
			return  ;
		}
		int head = byteToInt(dat[0]);
		if(!checkBytesKey(dat))
			return ;
		switch(head){
		case 0:
			//确认是否在线
			UDPSender.Send(new byte[]{(byte) MARK_IAMONLINE},
					1, dp.getAddress(), Const.PORT_UDP_CLIENT);
			Log.addLog("已回复UDP的询问在线请求!");
			break;
		case 1:
			//鼠标
			if(datLength > 7){
				MouseKeyboard.mouse(byteToInt(dat[7]));
			}else{
				Log.addLog("UDP发送来的鼠标请求数据非法!");
			}
			break;
		case 2:
			//键盘
			if(datLength > 8){
				MouseKeyboard.keyboard(byteToInt(dat[7]),byteToInt(dat[8]));
			}else{
				Log.addLog("UDP发送来的键盘请求数据非法!");
			}
			break;
		case 3:
			//文件
			if(datLength > 7){
				String path = new String(dat,7,dat.length-7);
				FileManager.fileList(path,dp.getAddress());
			}else{
				Log.addLog("UDP发送来的文件列表请求数据非法!");
			}
			break;
		case 4:
			//下载文件
			if(datLength > 7){
				String path = new String(dat,7,dat.length-7);
				FileManager.downloadFile(path,dp.getAddress());
			}else{
				Log.addLog("UDP发送来的下载文件请求数据非法!");
			}
			break;
		}
	}
	
	private static boolean checkBytesKey(byte[] dat){
		return true;
	}
	
	private static int byteToInt(byte b){
		int val = b;
		val = ( 256 + val ) % 256;
		return val;
	}
}
