package hangxingliu.java.atk.test;

import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.BorderLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Font;

public class RecordKeyCode extends JFrame {

	private static final long serialVersionUID = -8321331644391689145L;
	private JLabel tvRes;
	private JLabel label;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		RecordKeyCode frame = new RecordKeyCode();
		frame.setVisible(true);				
	}

	/**
	 * Create the frame.
	 */
	public RecordKeyCode() {
		setAlwaysOnTop(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent ke) {
				tvRes.setText("KeyCode:".concat(Integer.toString(ke.getKeyCode())));
			}
		});
		setTitle("Record KeyCode");
		setBounds(100, 100, 450, 109);
		
		tvRes = new JLabel("KeyCode:");
		tvRes.setFont(new Font("Comic Sans MS", Font.BOLD, 20));
		getContentPane().add(tvRes, BorderLayout.NORTH);
		
		label = new JLabel("Press Any Key To Show Keycode");
		getContentPane().add(label, BorderLayout.SOUTH);

	}

}
