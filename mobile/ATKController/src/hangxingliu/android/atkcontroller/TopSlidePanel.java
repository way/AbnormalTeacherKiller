package hangxingliu.android.atkcontroller;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

public class TopSlidePanel extends LinearLayout implements android.view.View.OnClickListener {

	private HorizontalScrollView sv;
	private LinearLayout lMain;
	
	private Context context;
	
	private onSelectListener l = null;
	
	private int lastSelect = -1;
	
	public TopSlidePanel(Context context) {
		super(context);
		this.context = context;
		
		initView();
	}
	
	private void initView(){
		sv = new HorizontalScrollView(context);
		sv.setLayoutParams(new LinearLayout.LayoutParams(-1, 80));
		
		lMain = new LinearLayout(context);
		lMain.setOrientation(HORIZONTAL);
		
		sv.addView(lMain);
		
		this.addView(sv);
	}
	
	public void setOnSelectListener(onSelectListener l){
		this.l = l;
	}
	
	public void addButton(int id,String title,Drawable icon,boolean index){
		Button btn = new Button(context);
		btn.setText(title);
		btn.setTag(id);
		btn.setCompoundDrawablesWithIntrinsicBounds(icon, null, null, null);
		btn.setLayoutParams(new LinearLayout.LayoutParams(-2, 78));
		btn.setOnClickListener(this);
		if(index)
			lastSelect = id;
		
		lMain.addView(btn);
	}

	@Override
	public void onClick(View view) {
		if(l == null)return ;
		int id = (Integer) view.getTag();
		l.onUnSelect(lastSelect);
		lastSelect = id;
		l.onSelect(id);
	}
	
	
	public static interface onSelectListener{
		
		public void onSelect(int id);
		public void onUnSelect(int id);
		
	}
}
