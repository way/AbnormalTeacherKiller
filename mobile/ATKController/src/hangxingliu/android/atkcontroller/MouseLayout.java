package hangxingliu.android.atkcontroller;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

@SuppressLint("ViewConstructor")
public class MouseLayout extends LinearLayout implements android.view.View.OnClickListener {

	private Activity context;
	
	private ScrollView sv;
	private LinearLayout lMain;
	
	private Button btnLeft,btnRight,btnDouble,btnUp3,btnUp2,btnUp1,btnDown3,btnDown2,btnDown1;
	private TextView tvLine1,tvLine2;
	
	public MouseLayout(Activity context) {
		super(context);
		this.context = context;
		
		initView();
	}
	
	private void initView(){
		this.setOrientation(VERTICAL);
		
		sv = new ScrollView(context);
		lMain = new LinearLayout(context);
		lMain.setOrientation(VERTICAL);
		sv.addView(lMain);
		this.addView(sv);
		
		tvLine1 = new TextView(context);
		tvLine1.setText("----------");
		tvLine1.setTextSize(35.0f);
		
		tvLine2 = new TextView(context);
		tvLine2.setText("----------");
		tvLine2.setTextSize(35.0f);
		
		
		btnLeft = GenButton.gen(context, "左键",0);
		btnLeft.setCompoundDrawablesWithIntrinsicBounds(new VDrawable(getResources(), 20, 45, R.drawable.left), null, null, null);
		btnLeft.setOnClickListener(this);
		
		btnRight = GenButton.gen(context, "右键",1);
		btnRight.setCompoundDrawablesWithIntrinsicBounds(new VDrawable(getResources(), 20, 45, R.drawable.right), null, null, null);
		btnRight.setOnClickListener(this);
		
		btnDouble = GenButton.gen(context, "双击",2);
		btnDouble.setOnClickListener(this);
		
		btnUp3 = GenButton.gen(context, "滚上11",3);
		btnUp3.setOnClickListener(this);
		btnUp2 = GenButton.gen(context, "滚上5",4);
		btnUp2.setOnClickListener(this);
		btnUp1 = GenButton.gen(context, "滚上2",5);
		btnUp1.setOnClickListener(this);
		
		btnDown3 = GenButton.gen(context, "滚下11",6);
		btnDown3.setOnClickListener(this);
		btnDown2 = GenButton.gen(context, "滚下5",7);
		btnDown2.setOnClickListener(this);
		btnDown1 = GenButton.gen(context, "滚下2",8);
		btnDown1.setOnClickListener(this);
		
		lMain.addView(btnLeft);
		lMain.addView(btnRight);
		lMain.addView(btnDouble);
		
		lMain.addView(tvLine1);
		
		lMain.addView(btnUp3);
		lMain.addView(btnUp2);
		lMain.addView(btnUp1);
		
		lMain.addView(tvLine2);
		
		lMain.addView(btnDown1);
		lMain.addView(btnDown2);
		lMain.addView(btnDown3);
	}
	

	private void showUnLinkDialog(){
		showDialog("无连接!","没有可用ATK受控端可以连接!");
	}
	
	private void showExceptionDialog(Exception e){
		String message = e.getClass().getCanonicalName().concat("\n");
		if(e.getMessage()!=null)
			message = message.concat(e.getMessage());
		showDialog("出错", message);
	}
	
	private void showDialog(final String title,final String message){
		context.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				new AlertDialog.Builder(context).setTitle(title)
					.setMessage(message).setPositiveButton("确定", null).create().show();
			}
		});
	}

	@Override
	public void onClick(View view) {
		final int action = (Integer) view.getTag();
		if(LinkInfo.address == null){
			showUnLinkDialog();
			return ;
		}
		new Thread(new Runnable() {
			@Override
			public void run() {
				Exception e = UDPSender.Send(Cmd.genMouse((byte) action),8, LinkInfo.address.getHostAddress(), 9711);
				if(e != null)
					showExceptionDialog(e);
			}
		}).start();
	}
}
