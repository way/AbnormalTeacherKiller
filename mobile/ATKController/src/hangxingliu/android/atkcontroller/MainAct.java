package hangxingliu.android.atkcontroller;

import hangxingliu.android.atkcontroller.TopSlidePanel.onSelectListener;
import android.os.Bundle;
import android.app.Activity;
import android.content.res.Resources;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;

public class MainAct extends Activity implements onSelectListener {

	private LinearLayout lRoot;
	
	private TopSlidePanel lTop;
	private LinearLayout lBottom;
	
	private int layoutsLength = 4;
	private LinearLayout[] layouts = new LinearLayout[layoutsLength];
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		LinkInfo.address = null;
		
		initView();
	}
	
	private void initView(){
		Resources res = getResources();
		
		lRoot = new LinearLayout(this);
		lRoot.setOrientation(LinearLayout.VERTICAL);
		
		lTop = new TopSlidePanel(this);
		lTop.setOnSelectListener(this);
		lTop.addButton(0, "����", new VDrawable(res, 45, 45, R.drawable.link), true);
		lTop.addButton(1, "���", new VDrawable(res, 20, 45, R.drawable.mouse), false);
		lTop.addButton(2, "����", new VDrawable(res, 45, 45, R.drawable.keyboard), false);
		lTop.addButton(3, "�ļ�", new VDrawable(res, 45, 45, R.drawable.file), false);
		
		lBottom = new LinearLayout(this);
		lBottom.setOrientation(LinearLayout.VERTICAL);
		
		layouts[0] = new LinkLayout(this);
		layouts[0].setVisibility(View.VISIBLE);
		layouts[1] = new MouseLayout(this);
		layouts[1].setVisibility(View.GONE);
		
		lBottom.addView(layouts[0]);
		lBottom.addView(layouts[1]);
		
		lRoot.addView(lTop);
		lRoot.addView(lBottom);
		
		this.setContentView(lRoot);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public void onSelect(int id) {
		if(layouts[id] != null)
			layouts[id].setVisibility(View.VISIBLE);
	}

	@Override
	public void onUnSelect(int id) {
		if(layouts[id] != null)
			layouts[id].setVisibility(View.GONE);
	}

}
